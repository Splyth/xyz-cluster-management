# XYZ Cluster Management

This is a Proof Concept for managing k8s cluster deployments in EKS via GitOps using GitLab Pipelines and helm

It manages the cluster built by: https://gitlab.com/Splyth/xyz-cluster-deployment and uses a test application: https://gitlab.com/Splyth/xyz-cluster-application 

## Prerequisites

- AWS Account with EKS Cluster Deployment Permission

## Usage

1. Create Changes
2. Push Changes
3. [Check Pipeline](https://gitlab.com/Splyth/xyz-cluster-management/-/pipelines)
4. If you wish to actually push changes to cluster click `sync` job and press the play icon once the `diff` job is finished

## New Project Setup
This section is if you want to fork this and attempt to run it yourself.

1. [Install K8s Agent for Gitlab](https://docs.gitlab.com/ee/user/clusters/agent/install/)
2. In your Gitlab Repo goto: Settings > CI/CD. 
3. Expand Variables.
4. Add KUBECONTEXT variable (path to k8s agent)
EXAMPLE mine is: `Splyth/xyz-cluster-management:eks-agent`